import { NgModule } from '@angular/core';
import {MatIconModule} from '@angular/material/icon';
import {TableModule} from 'primeng/table';
import {CalendarModule} from 'primeng/calendar';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ToastModule} from 'primeng/toast';
import {ToolbarModule} from 'primeng/toolbar';
import {DialogModule} from 'primeng/dialog';
import {InputTextareaModule} from 'primeng/inputtextarea';
import { HttpClientModule } from '@angular/common/http';
import {CardModule} from 'primeng/card';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatIconModule,
    TableModule,
    CalendarModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    ConfirmDialogModule,
    ToastModule,
    ToolbarModule,
    DialogModule,
    InputTextareaModule,
    HttpClientModule,
    CardModule,
    MatCardModule,
    
  ],
  exports : [
    CommonModule,
    MatIconModule,
    TableModule,
    CalendarModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    ConfirmDialogModule,
    ToastModule,
    ToolbarModule,
    DialogModule,
    InputTextareaModule,
    HttpClientModule,
    CardModule,
    MatCardModule
  ]
})
export class SharedModule { }
