import { NgModule } from '@angular/core';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [PagesComponent],
  imports: [
    PagesRoutingModule,
    CoreModule,
    SharedModule
  ]
})
export class PagesModule { }
