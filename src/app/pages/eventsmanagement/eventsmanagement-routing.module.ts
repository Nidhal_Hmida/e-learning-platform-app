import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventsmanagementComponent } from './eventsmanagement.component';

const routes: Routes = [{
  path : '' , component : EventsmanagementComponent ,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsmanagementRoutingModule { }
