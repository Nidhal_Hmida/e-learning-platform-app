import { Component, OnInit } from '@angular/core';
import {  FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ELearningPlatformService } from 'src/app/core/services/e-learning-platform.service';
import { Event } from 'src/app/models/event.model';
import { DatePipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-eventsmanagement',
  templateUrl: './eventsmanagement.component.html',
  styleUrls: ['./eventsmanagement.component.scss']
})
export class EventsmanagementComponent implements OnInit {
   
  endPoint= "events"
  operation : boolean  
  form : FormGroup
  id : number
  events : Event[]=[]
  dataDialog: boolean;
  event : Event = new Event()
  submitted: boolean;
  
  selectedFile : any = "../../../assets/e-learning.png" ;
  
  
  constructor(private domSanitizer: DomSanitizer,private messageService: MessageService,private formBuilder : FormBuilder,private service :ELearningPlatformService,private confirmationService: ConfirmationService) { }


  ngOnInit(): void {
    this.form = this.formBuilder.group({
      'title' : [null,Validators.compose([Validators.required, Validators.pattern("^[a-zA-Zéàè:' ]+$"),this.noWhitespaceValidator])],
      'description' : [null,Validators.compose([Validators.required,this.noWhitespaceValidator])],
      'photo' : [null,Validators.compose([Validators.required])] ,
      'datetime' : [null,Validators.compose([Validators.required])],    
    })
    this.getEvents()
  }

  fileChange(event:any)
  {

  if(event.target.files != undefined)
  {
    var nom=new String(event.target.files[0].name); 
    if(nom.indexOf("png")== -1 && nom.indexOf("PNG")== -1 && nom.indexOf("jpg")== -1 && nom.indexOf("jpeg")== -1&& nom.indexOf("JPG")== -1 && nom.indexOf("JPEG")== -1)
    alert("svp vous devez importer une image ")
    else
  {
  var myReader:FileReader = new FileReader();
  myReader.onloadend = (e) => {
	this.selectedFile = myReader.result;
  this.form.get("photo").setValue(this.selectedFile)

  }
  myReader.readAsDataURL(event.target.files[0]);
  }
  }
}

  resetValues()
  {
    this.form.reset()
  }

  openNew() {
  
    this.submitted = false;
    this.dataDialog = true;
    this.form.reset()
    this.operation = false
}


onModify(event:Event)
{
  this.event=event;
  this.dataDialog = true;
   this.operation= true
   this.id=event.id
   this.form.get("title").setValue(this.event.title)
   this.form.get("description").setValue(this.event.description)
   this.form.get("datetime").setValue(new Date(this.event.datetime))
   this.selectedFile=event.photo
   this.form.get("photo").setValue(this.selectedFile)
}

hideDialog() {
  this.dataDialog = false;
  this.submitted = false;
  this.form.reset()
}


  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }


  getEvents()
  {
    this.service.get(this.endPoint).subscribe((data: Event [])=>{  
      this.events = data
         });
  }

  security(image)
  {
    
     return this.domSanitizer.bypassSecurityTrustUrl(image);
     
    }
  
  
  onSubmit()
  {
    if(!this.operation)
    {
        this.event =new Event(this.form.get("title").value,this.form.get("description").value,new DatePipe("fr-FR").transform(this.form.get("datetime").value, "yyyy-MM-dd HH:mm"),this.form.get("photo").value,null)
        this.service.add(this.endPoint,this.event).subscribe((data: Event )=>{
          if(data != null)
          {
        
            this.hideDialog()
            this.messageService.add({severity:'success', summary: 'Succés', detail: "The event is added", life: 3000}); 
            this.events.splice(0,0,data)
            this.event = new Event()
          }
          else
          this.messageService.add({severity:'error', summary: 'Probléme', detail: "The event is not added, please check the information entered", life: 3000});
        });  
  }

    else
    {
        this.event =new Event(this.form.get("title").value,this.form.get("description").value,new DatePipe("fr-FR").transform(this.form.get("datetime").value, "yyyy-MM-dd HH:mm"),this.form.get("photo").value,this.id)
       
        this.service.modify(this.endPoint,this.event).subscribe((data: Event )=>{
        if(data != null)
        {
        this.operation = false
        this.hideDialog()
        this.messageService.add({severity:'success', summary: 'Succés', detail: "Event informations is changed", life: 3000});
        var indice = this.service.getData(this.event.id,this.events)
        this.events.splice(indice,1)
        this.events.splice(indice,0,this.event)
         this.event = new Event()
        
      }
        else  
        this.messageService.add({severity:'error', summary: 'Probléme', detail: "The event informations is not modified, please check the informations entered", life: 3000});
      });      
    }
     }

     onDelete(event:Event)
     {
      this.event=event
      this.confirmationService.confirm({
       message: "Are you sure you want to delete this event " + event.title + ' ?',
       header: 'Confirmation',
       icon: 'pi pi-exclamation-triangle',
   });
     }
 
 
     delete()
     {
       this.service.delete(this.endPoint,this.event.id).subscribe((data)=>{
         var indice = this.service.getData(this.event.id,this.event)
         this.events.splice(indice,1)
         this.messageService.add({severity:'success', summary: 'Succés', detail: "The event is deleted", life: 3000});
         this.close()
       },
           (error) => {
             this.messageService.add({severity:'error', summary: 'Probléme', detail: "the event is not deleted", life: 3000});
             this.close()
           })        
     }
 
     close()
     {
       this.confirmationService.close()
     }

    }


