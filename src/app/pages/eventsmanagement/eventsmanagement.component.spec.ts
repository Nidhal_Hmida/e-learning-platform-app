import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsmanagementComponent } from './eventsmanagement.component';

describe('EventsmanagementComponent', () => {
  let component: EventsmanagementComponent;
  let fixture: ComponentFixture<EventsmanagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventsmanagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
