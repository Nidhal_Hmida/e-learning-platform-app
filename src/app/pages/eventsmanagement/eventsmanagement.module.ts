import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { EventsmanagementRoutingModule } from './eventsmanagement-routing.module';
import { EventsmanagementComponent } from './eventsmanagement.component';


@NgModule({
  declarations: [EventsmanagementComponent],
  imports: [
    SharedModule,
    EventsmanagementRoutingModule
  ]
})
export class EventsmanagementModule { }
