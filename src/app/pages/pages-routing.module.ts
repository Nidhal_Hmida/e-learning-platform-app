import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';

const routes: Routes = [{
  path : '' , component : PagesComponent 
 , children : [
{
  path : 'eventsschedule' ,  loadChildren :() => import('./eventsmanagement/eventsmanagement.module').then(m =>m.EventsmanagementModule)
},
]},
{path :'',redirectTo :'eventsschedule', pathMatch : 'full'},
{path : '**',redirectTo :'eventsschedule'}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
