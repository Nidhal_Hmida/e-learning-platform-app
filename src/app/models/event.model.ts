export class Event {
    id : number;
    title : string
    description : string
    datetime : string
    photo : string

    constructor(title?:string,description?:string,datetime?:string,photo?:string,id?:number)
    {
      this.id = id
      this.title = title
      this.description = description
      this.datetime = datetime
      this.photo = photo
    }
}
