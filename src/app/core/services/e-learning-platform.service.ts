import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ELearningPlatformService {
  
 
  
  optionRequete = {
    headers: new HttpHeaders({ 
     
      'Content-Type':  'application/json',
      'Access-Control-Allow-Credentials' : 'true',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
    })  
  };
  
  path:string ="https://e-learning-platform-api.herokuapp.com/api/"

  
  constructor(private httpClient: HttpClient) {}

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    
      // Server-side errors
      errorMessage = `Error Code: ${error.status}`;
    return (errorMessage);
  }
  
  get(endPoint)
  {

    return this.httpClient.get(this.path+endPoint,this.optionRequete).pipe(shareReplay(1));
  }

  add(endPoint,data)
  {
    
    return this.httpClient.post(this.path+endPoint,data,this.optionRequete);
  }

  modify(endPoint,data)
  {
    console.log(data)
    return this.httpClient.put(this.path+endPoint+"/"+data.id,data,this.optionRequete);   
  }

  delete(endPoint,id : number)
  {
    
   return  this.httpClient.delete(this.path+endPoint+"/"+id,this.optionRequete)
  }

  getData(id ,elements)
  {
   
    for(var i=0;i<elements.length;i++) 
    {
      if(id== elements[i]["id"])
      return(i)
    }
    return(-1)
  }

}
