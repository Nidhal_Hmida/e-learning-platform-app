import { TestBed } from '@angular/core/testing';

import { ELearningPlatformService } from './e-learning-platform.service';

describe('ELearningPlatformService', () => {
  let service: ELearningPlatformService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ELearningPlatformService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
