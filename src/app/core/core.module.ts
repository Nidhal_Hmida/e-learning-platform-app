import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SharedModule } from '../shared/shared.module';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ELearningPlatformService } from './services/e-learning-platform.service';



@NgModule({
  declarations: [NavbarComponent],
  providers : [{provide : ELearningPlatformService , useClass : ELearningPlatformService},
    {provide : ConfirmationService},
    {provide : MessageService}],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports : [NavbarComponent]

})
export class CoreModule { }
